import React, { Component, Fragment } from "react";
// import BoundingBox from "./BoundingBox";
import { withRouter  } from "react-router-dom";
import Image from "./Images";
import "./App.css";

class PhotoGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        "https://www.galeriecaratsch.com/fileadmin/user_upload/_bilder/_home_slider/Bild3_Self_Portraits_View_4_20140207-0019-2.jpg",
        "https://artlogic-res.cloudinary.com/w_1880,h_1064,c_fill,f_auto,fl_lossy/ws-inglebygallery/usr/images/pages/slideshow_data/1/garry-fabian-miller_jm2_3703-2_press.jpg",
        "https://www.oxotower.co.uk/wp-content/uploads/2013/11/Arthur-Laidlaw-Photography-634-400.jpg"
      ],
      showImg: true,
    };
  }

  handleCLick = () => {
    this.setState({
      showImg: false
    })
    this.props.history.push({
      pathname: '/boundryImage',
      state: { ...this.states }
    
    })
  };

  render() {
    return (
      <Fragment>
        {this.state.showImg?
       <div className='images'>
         <h2> Photo Gallery </h2>
         <h2>to upload image for boundries </h2>
          <button
        type="button"
        className="btn btn-primary"
        onClick={this.handleCLick}
      >click</button> 
      {this.state.images.map(item => (
        <Image value={item}/>
      ))}
       </div> : '' }
     </Fragment>
    );
  }
}

export default withRouter(PhotoGallery);
