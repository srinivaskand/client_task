import React, {Component} from 'react'
import Bounding  from 'react-bounding-box'
import './App.css';

class Coordinates extends Component{
    constructor(props){
        super(props);
        this.state ={
            showbound: false,
            xCoord:'',
            yCoord:'',
              image: '',
              box:[],
              boxes :[]
              
        }
    }
 
   onImage = e => {
        if (e.target.files && e.target.files[0]) {
          let reader = new FileReader();
          reader.onload = e => {
            this.setState({ 
                image: e.target.result
            } );
          };
          reader.readAsDataURL(e.target.files[0]);
        }
      };
      handleCoord = event => {
        let boxes = this.state.box;
        boxes[parseInt(event.target.name)] = event.target.value;
        this.setState({
               boxes : [boxes]
              });
      };
    handleCLick = () => {
       this.setState({
        showbound: true
       })
      };
      handleLoad=()=>{
        var c = document.getElementById('myCanvas');
        var ctx = c.getContext("3d");
        var img = document.getElementById('target');
        ctx.drawImage(img);
        ctx.strokeStyle = 'white';
        ctx.strokeRect(50, 30, 200, 200);
}
      
    render(){
        return(
            <div>
          <div>
          <button
            type="button"
            className="btn btn-primary "
            onClick={this.handleCLick}
          >download image
          </button>
        </div>
            <div>
          <span>
            <input
              type="file"
              onChange={this.onImage}
              className="filetype"
              id="single"
            />
          </span>
        </div>

        <div className="firstCord">
          {" "}
          X1 :{" "}
          <input type="number" onChange={this.handleCoord} />
          Y1 :{" "}
          <input
            type="number"
            onChange={this.handleCoord}
          />{" "}
        </div>


        <div className="secondCord">
          {" "}
          X2 :{" "}
          <input type="number" onChange={this.handleCoord} />
          Y2 :{" "}
          <input
            type="number"
            onChange={this.handleCoord}
          />{" "}
        </div>
        {this.state.showbound ?  <Bounding id="custom" image={this.state.image} onSelected={this.showCoordinates}
             boxes={this.state.boxes}
             options={this.state.options}
            /> : ''}
            </div>
        )
    }
}

export default Coordinates
