import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Coordinates from "./Coordinates";
import PhotoGallery from "./PhotoGallery";



const Routes =()=> (
    <Router>
        <Route path="/" component={PhotoGallery} />
        <Route path="/boundryImage" component={Coordinates} />
    </Router>
  )
 export default Routes;