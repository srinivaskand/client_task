import React from 'react'
import Bounding  from 'react-bounding-box'
import './App.css';

const values = {
    boxes: [
      [0, 0, 250, 250],
      [300, 0, 250, 250],
    ],
  };

class Images extends React.Component {   
    render() {
      return (
        <div className="images"> 
         <Bounding image={this.props.value} boxes={values.boxes}
             />
        </div>
      );
    }
  }

export default Images 